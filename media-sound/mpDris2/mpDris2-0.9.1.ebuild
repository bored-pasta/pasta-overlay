# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{9,10} )
inherit python-r1 autotools

DESCRIPTION="MPRIS V2.1 support for mpd"
HOMEPAGE="https://github.com/eonpatapon/mpDris2"

SRC_URI="https://github.com/eonpatapon/${PN}/archive/refs/tags/${PV}.tar.gz"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="
	>=dev-python/dbus-python-1.3.2[$PYTHON_USEDEP]
	>=dev-python/pygobject-3.42.2[$PYTHON_USEDEP]
	>=dev-python/python-mpd-3.0.5[$PYTHON_USEDEP]"
DEPEND="${RDEPEND}"
DOCS="AUTHORS COPYING INSTALL NEWS README README.md"

src_prepare() {
	default
	eautoreconf
}
